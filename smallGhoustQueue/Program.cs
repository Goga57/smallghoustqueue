﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
// i fix somethink here in the branch
// i merged and change main branch
namespace smallGhoustQueue
{
    class Program
    {
        private static SaveForMultiThreadQueue<int> exampleQueue = new SaveForMultiThreadQueue<int>();

        static void Main(string[] args)
        {

            #region SaveForMultiThreadQueue test

            // Create 8 Threads  that will use ours SaveForMultiThreadQueue as readers
            for (int i = 0; i < 8; i++)
            {
                Thread newThread = new Thread(new ThreadStart(ThreadProcGetQueue));
                newThread.Name = String.Format("Thread{0}", i + 1);
                newThread.Start();
            }

            // create pusher (it will push elements in our queue)
            Thread newThreadPusher = new Thread(new ThreadStart(ThreadProcSetQueue));
            newThreadPusher.Name = String.Format("Thread{0}", "pusher Thread");
            newThreadPusher.Start();

            Console.ReadLine();

            #endregion

        }

        #region functions for threads
        private static void ThreadProcGetQueue()
        {
            for (int i = 0; i < 8; i++)
            {
                exampleQueue.pop();
            }
            Console.WriteLine("{0} has finished!!!",Thread.CurrentThread.Name);
        }

        private static void ThreadProcSetQueue()
        {
            for (int i = 0; i < 64; i++)
            {
                exampleQueue.push(42);
            }
            Console.WriteLine("{0} has finished!!!", Thread.CurrentThread.Name);
        }

        #endregion

    }

    #region class SaveForMultiThreadQueue<T>
    class SaveForMultiThreadQueue<T>
    {
        private Mutex mWrite,mRead;
        private ManualResetEventSlim mWaitElement;
        Queue<T> innerQueue;
        public SaveForMultiThreadQueue()
        {
            innerQueue = new Queue<T>();
            mWrite = new Mutex();
            mRead = new Mutex();
            mWaitElement = new ManualResetEventSlim(false);
        }

        public void push(T t)
        {
            
            mWrite.WaitOne();

                innerQueue.Enqueue(t);
                mWaitElement.Set();

            mWrite.ReleaseMutex();
        }

        public T pop()
        {
            T ret;
            mRead.WaitOne();
                mWaitElement.Wait();
                ret = innerQueue.Dequeue();
                if (innerQueue.Count==0)
                    mWaitElement.Reset();
            mRead.ReleaseMutex();
            return ret;
        }
    }

    #endregion


}
